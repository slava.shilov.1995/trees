#include <iostream>
#include <time.h>
#include <vector>
#include <map>
#include <chrono>
#include <iomanip>

#define TR2 pair<TRN*, TRN*>
#define SP2 pair<SPN*, SPN*>
#define NOW std::chrono::system_clock::now()
#define SW setw(12)

using namespace std;

struct TRN {
	TRN * left;
	TRN * right;

	int x;
	int y;

	TRN(int x, int y) {
		this->x = x;
		this->y = y;

		left = nullptr;
		right = nullptr;
	}
};

struct TREAP {
	TRN * root = nullptr;

	TREAP() {
		root = nullptr;
	}

	TREAP(int x, int y) {
		this->root = new TRN(x, y);
	}

	TREAP(TRN * root) {
		this->root = root;
	}

	void clear() {
		while (root != nullptr) {
			remove(root->x);
		}
	}

	TR2 split(TRN * t, int k) {
		if (t == nullptr) {
			return TR2(nullptr, nullptr);
		} else if (k > t->x) {
			TR2 p = split(t->right, k);
			t->right = p.first;
			return TR2(t, p.second);
		} else {
			TR2 p = split(t->left, k);
			t->left = p.second;
			return TR2(p.first, t);
		}
	}

	TRN * merge(TRN * t1, TRN * t2) {
		if (t2 == nullptr) {
			return t1;
		}
		if (t1 == nullptr) {
			return t2;
		} else if (t1->y > t2->y) {
			t1->right = merge(t1->right, t2);
			return t1;
		} else {
			t2->left = merge(t1, t2->left);
			return t2;
		}
	}

	void insert(int x, int y) {
		TRN * k = new TRN(x, y);
		if (root == nullptr) {
			root = k;
		} else {

			if (k->y > root->y) {
				TR2 p = split(root, k->x);
				k->right = p.second;
				k->left = p.first;
				root = k;
				return;
			}

			TRN * _c = root;
			bool r = true;
			TRN * c = root;
			while (c != nullptr && c->y >= k->y) {
				if (c->x > k->x) {
					_c = c;
					c = c->left;
					r = false;
				} else {
					_c = c;
					c = c->right;
					r = true;
				}
			}
			TR2 p = split(c, k->x);
			k->left = p.first;
			k->right = p.second;
			if (r) {
				_c->right = k;
			} else {
				_c->left = k;
			}
			/*if (k->y > root->y) {
			root = k;
			}*/
		}

	}

	void remove(int x) {
		if (root != nullptr) {
			TRN * _c = root;
			bool r = true;
			TRN * c = root;
			while (c != nullptr && c->x != x) {
				if (c->x > x) {
					_c = c;
					c = c->left;
					r = false;
				} else {
					_c = c;
					c = c->right;
					r = true;
				}
			}
			if (c != nullptr) {
				
				TRN * n = merge(c->left, c->right);
				if (c == root) {
					root = n;
				} else {
					if (r) {
						_c->right = n;
					} else {
						_c->left = n;
					}
				}
				
				delete c;
			}
		}
	}

	//SVETE: PODOZRITEL'NO BISTRO
	TRN * _find(int k) {
		TRN * c = root;
		while (c != nullptr && c->x != k) {
			if (c->x > k) {
				c = c->left;
			} else {
				c = c->right;
			}
		}
		return c;
	}

	void print() {
		cout << "Treap:" << endl;
		print(root, 0);
	}

	void print(TRN * p, int level) {
		if (p) {
			print(p->right, level + 1);

			for (int i = 0; i < level; ++i) cout << "  ";
			cout << "(" << p->x << ";" << p->y << ")" << endl;
			print(p->left, level + 1);
		}
	}
};

struct SPN {
	int x;
	int y;

	SPN * parent;
	SPN * left;
	SPN * right;

	SPN(int x, int y) {
		this->x = x;
		this->y = y;
		parent = nullptr;
		left = nullptr;
		right = nullptr;
	}

	/*~SPN() {
		if (right) delete right;
		if (left) delete left;
	}*/

};

struct SPLAY {
	SPN * root;

	SPLAY() {
		root = nullptr;
	}

	SPLAY(SPN * root) {
		this->root = root;
	}

	void clear() {
		while (root) {
			remove(root->x);
		}
	}

	void _set_parent(SPN * c, SPN * p) {
		if (c != nullptr) {
			c->parent = p;
		}
	}

	void _keep_parent(SPN * p) {
		_set_parent(p->left, p);
		_set_parent(p->right, p);
	}

	void _rotate(SPN * p, SPN * c) {
		SPN * g = p->parent;
		if (g != nullptr) {
			if (g->left == p) {
				g->left = c;
			} else {
				g->right = c;
			}
		}
		if (p->left == c) {
			p->left = c->right;
			c->right = p;
		} else {
			p->right = c->left;
			c->left = p;
		}
		_keep_parent(c);
		_keep_parent(p);
		c->parent = g;
	}

	SPN * splay(SPN * v) {
		if (v->parent == nullptr) {
			return v;
		}
		SPN * p = v->parent;
		SPN * g = p->parent;
		if (g == nullptr) {
			_rotate(p, v);
			return v;
		} else {
			if ((g->left == p) == (p->left == v)) {
				_rotate(g, p);
				_rotate(p, v);
			} else {
				_rotate(p, v);
				_rotate(g, v);
			}
			return splay(v);
		}
	}

	SPN * find(SPN * v, int x) {
		if (v == nullptr) {
			return nullptr;
		}
		if (x == v->x) {
			return splay(v);
		}
		if (x < v->x && v->left != nullptr) {
			return find(v->left, x);
		}
		if (x > v->x && v->right != nullptr) {
			return find(v->right, x);
		}
		return splay(v);
	}

	SP2 split(SPN * v, int x) {
		if (v == nullptr) {
			return SP2(nullptr, nullptr);
		}
		v = find(v, x);
		if (v->x == x) {
			_set_parent(v->left, nullptr);
			_set_parent(v->right, nullptr);
			return SP2(v->left, v->right);
		}
		if (v->x < x) {
			SPN * r = v->right;
			v->right = nullptr;
			_set_parent(r, nullptr);
			return SP2(v, r);
		} else {
			SPN * l = v->left;
			v->left = nullptr;
			_set_parent(l, nullptr);
			return SP2(l, v);
		}
	}

	void insert(int x, int y) {
		SP2 p = split(root, x);
		root = new SPN(x, y);
		root->left = p.first;
		root->right = p.second;
		_keep_parent(root);
	}

	SPN * merge(SPN * l, SPN * r) {
		if (r == nullptr) {
			return l;
		}
		if (l == nullptr) {
			return r;
		}
		r = find(r, l->x);
		r->left = l;
		l->parent = r;
		return r;
	}

	void remove(int x) {
		SPN * v = find(root, x);
		_set_parent(v->left, nullptr);
		_set_parent(v->right, nullptr);
		root = merge(v->left, v->right);
		delete v;
	}

	void print() {
		cout << "Splay:" << endl;
		print(root, 0);
	}

	void print(SPN * p, int level) {
		if (p) {
			print(p->right, level + 1);

			for (int i = 0; i < level; ++i) cout << "  ";
			cout << "(" << p->x << ";" << p->y << ")" << endl;
			print(p->left, level + 1);
		}
	}
};

void randomize_vector(vector<pair<int, int>> & v) {
	int j;
	pair<int, int> c;
	for (int i = 0; i < v.size(); ++i) {
		j = rand() % v.size();
		c = v[i];
		v[i] = v[j];
		v[j] = c;
	}
}

void main() {
	srand(time(NULL));

	int mn = 100;
	int mx = 100000;

	int num_insertions = 100000;

	int ym = 100;
	
	cout << "Wait";

	chrono::time_point<chrono::system_clock> start, end;
	double time;

	int num_tests = 3;
	int num_n = 0; for (int i = mn; i <= mx; i *= 10, ++num_n);

	//[struct][operation][N]
	vector<vector<double>> tt(3, vector<double>(num_n, 0)), st(3, vector<double>(num_n, 0)), mt(3, vector<double>(num_n, 0));

	for (int tst = 0; tst < num_tests; ++tst) {
		num_n = 0;
		for (int n = mn; n <= mx; n *= 10) {
			vector<pair<int, int>> data;
			int base_data_size = n;
			int insert_data_size = num_insertions;
			int data_size = base_data_size + insert_data_size;
			for (int i = 0; i < data_size; ++i) {
				data.emplace_back(i, rand() % ym);
			}
			randomize_vector(data);

			TREAP t;
			SPLAY s;
			map<int, int> m;

			for (int i = 1; i < base_data_size; ++i) {
				int x = data[i].first;
				int y = data[i].second;
				t.insert(x, y);
				s.insert(x, y);
				m.emplace(x, y);
			}

			start = NOW;
			for (int i = base_data_size; i < data_size; ++i) {
				t.insert(data[i].first, data[i].second);
			}
			end = NOW;
			tt[0][num_n] += (chrono::duration<double>(end - start).count());
			//t.print();

			start = NOW;
			for (int i = base_data_size; i < data_size; ++i) {
				s.insert(data[i].first, data[i].second);
			}
			end = NOW;
			st[0][num_n] += (chrono::duration<double>(end - start).count());
			//s.print();

			start = NOW;
			for (int i = base_data_size; i < data_size; ++i) {
				m.emplace(data[i].first, data[i].second);
			}
			end = NOW;
			mt[0][num_n] += (chrono::duration<double>(end - start).count());

			/////////////////////////////////////////////////
			//randomize_vector(data);

			start = NOW;
			for (int i = base_data_size; i < data_size; ++i) {
				t._find(data[i].first);
			}
			end = NOW;
			tt[1][num_n] += (chrono::duration<double>(end - start).count());

			start = NOW;
			for (int i = base_data_size; i < data_size; ++i) {
				s.find(s.root, data[i].first);
			}
			end = NOW;
			st[1][num_n] += (chrono::duration<double>(end - start).count());

			start = NOW;
			for (int i = base_data_size; i < data_size; ++i) {
				m.find(data[i].first);
			}
			end = NOW;
			mt[1][num_n] += (chrono::duration<double>(end - start).count());

			/////////////////////////////////////////////////
			//randomize_vector(data);

			start = NOW;
			for (int i = base_data_size; i < data_size; ++i) {
				t.remove(data[i].first);
			}
			end = NOW;
			tt[2][num_n] += (chrono::duration<double>(end - start).count());

			start = NOW;
			for (int i = base_data_size; i < data_size; ++i) {
				s.remove(data[i].first);
			}
			end = NOW;
			st[2][num_n] += (chrono::duration<double>(end - start).count());

			start = NOW;
			for (int i = base_data_size; i < data_size; ++i) {
				m.erase(data[i].first);
			}
			end = NOW;
			mt[2][num_n] += (chrono::duration<double>(end - start).count());

			cout << ".";
			num_n++;

			t.clear();
			s.clear();
			m.clear();
			data.clear();
		}
	}
	cout << endl;

	for (int i = 0; i < num_n; ++i) {
		for (int j = 0; j < 3; ++j) {
			tt[j][i] /= num_n;
			st[j][i] /= num_n;
			mt[j][i] /= num_n;
		}
	}

	cout << endl << "Insert time:" << endl;
	cout << SW << "N" << SW << "TREAP" << SW << "SPLAY" << SW << "MAP" << endl;
	int i = 0;
	for (int n = mn; n <= mx; n *= 10) {
		cout << SW << n << SW << tt[0][i] << SW << st[0][i] << SW << mt[0][i] << endl;
		++i;
	}

	cout << endl << "Search time:" << endl;
	cout << SW << "N" << SW << "TREAP" << SW << "SPLAY" << SW << "MAP" << endl;
	i = 0;
	for (int n = mn; n <= mx; n *= 10) {
		cout << SW << n << SW << tt[1][i] << SW << st[1][i] << SW << mt[1][i] << endl;
		++i;
	}

	cout << endl << "Remove time:" << endl;
	cout << SW << "N" << SW << "TREAP" << SW << "SPLAY" << SW << "MAP" << endl;
	i = 0;
	for (int n = mn; n <= mx; n *= 10) {
		cout << SW << n << SW << tt[2][i] << SW << st[2][i] << SW << mt[2][i] << endl;
		++i;
	}

	cin.get();
}
